package com.template;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by: Sergey Volokh
 * Date: 5/13/2016
 * Time: 6:34 PM
 * Project: Spring MVC
 */
@SpringBootApplication
public class StartProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(StartProjectApplication.class, args);
    }

}
