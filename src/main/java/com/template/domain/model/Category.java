package com.template.domain.model;

import com.template.domain.IdModel;
import com.template.domain.model.enums.CategoryType;

import javax.persistence.*;

/**
 * Created by: Sergey Volokh
 * Date: 5/18/2016
 * Time: 6:31 PM
 * Project: Spring MVC
 */
@Entity
@Access(AccessType.FIELD)
@Table(name = "category", uniqueConstraints = @UniqueConstraint(columnNames = {"category_name","category_type"}))
public class Category implements IdModel{

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(name = "category_name")
    private String name;

    @Column(name = "category_description")
    private String description;

    @Column(name = "category_photo_url")
    private String photoUrl;

    @Column(name = "category_type")
    @Enumerated(EnumType.STRING)
    private CategoryType type;

    @OneToOne(mappedBy = "category")
    @PrimaryKeyJoinColumn
    private Blog blog;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CategoryType getType() {
        return type;
    }

    public void setType(CategoryType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
