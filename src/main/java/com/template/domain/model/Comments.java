package com.template.domain.model;

import com.template.domain.IdModel;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by: Sergey Volokh
 * Date: 5/18/2016
 * Time: 4:26 PM
 * Project: Spring MVC
 */
@Entity
@Table(name = "blog_comments")
@Access(AccessType.FIELD)
public class Comments implements IdModel {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(name = "comment")
    private String comment;

    @Temporal(TemporalType.DATE)
    private Date date;

    @ManyToOne
    private User user;

    @ManyToOne
    private Blog blog;

    @ManyToOne
    private News news;

    public Comments() {}

    public Comments(String comment) {
        this.comment = comment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
//
//    public Blog getBlog() {
//        return blog;
//    }
//
//    public void setBlog(Blog blog) {
//        this.blog = blog;
//    }
//
//    public News getNews() {
//        return news;
//    }
//
//    public void setNews(News news) {
//        this.news = news;
//    }
}
