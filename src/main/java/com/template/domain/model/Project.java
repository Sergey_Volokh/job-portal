package com.template.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.template.domain.IdModel;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by: Sergey Volokh
 * Date: 5/18/2016
 * Time: 3:57 PM
 * Project: Spring MVC
 */
@Entity
@Table(name = "projects")
@Access(AccessType.FIELD)
public class Project implements IdModel {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @NotEmpty
    @Size(min = 1)
    @Column(name = "project_name", nullable = false)
    private String name;

    @NotEmpty
    @Size(min = 10)
    @Column(name = "project_description", nullable = false)
    private String description;

//    @URL(message = "{message.url.resource.mustbe.photo}")
    @Column(name = "web_site_description")
    private String webSiteUrl;

//    @URL
    @Column(name = "project_photo_url")
    private String photoUrl;

    @Column(name = "required_skills")
    private String requiredSkills;

    @OneToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @OneToOne
    @JoinColumn(name = "rate_id")
    private Rate rate;

    @CreationTimestamp
    @Temporal(TemporalType.DATE)
    @Column(name = "created_at", nullable = false, insertable = true, updatable = false)
    private Date createdAt;

    @OneToMany(cascade = CascadeType.ALL)
    private List<News> projectNews = new ArrayList<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "project_members",
            joinColumns = @JoinColumn(name = "project_id"),
            inverseJoinColumns = @JoinColumn(name = "member_id")
    )
    private List<User> members = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "project_necessary_skill",
            joinColumns = @JoinColumn(name = "project_id"),
            inverseJoinColumns = @JoinColumn(name = "skill_id")
    )
    private List<Skill> necessarySkills = new ArrayList<>();

    @Column(name = "tags")
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Tags> tags;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getRequiredSkills() {
        return requiredSkills;
    }

    public void setRequiredSkills(String requiredSkills) {
        this.requiredSkills = requiredSkills;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
//
//    public List<User> getOwners() {
//        return owners;
//    }
//
//    public void setOwners(List<User> owners) {
//        this.owners = owners;
//    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }

    public Rate getRate() {
        return rate;
    }

    public void setRate(Rate rate) {
        this.rate = rate;
    }

    public String getWebSiteUrl() {
        return webSiteUrl;
    }

    public void setWebSiteUrl(String webSiteUrl) {
        this.webSiteUrl = webSiteUrl;
    }

    public List<News> getProjectNews() {
        return projectNews;
    }

    public void setProjectNews(List<News> projectNews) {
        this.projectNews = projectNews;
    }

    public List<Tags> getTags() {
        return tags;
    }

    public void setTags(List<Tags> tags) {
        this.tags = tags;
    }

    public List<Skill> getNecessarySkills() {
        return necessarySkills;
    }

    public void setNecessarySkills(List<Skill> necessarySkills) {
        this.necessarySkills = necessarySkills;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Project project = (Project) o;

        if (id != null ? !id.equals(project.id) : project.id != null) return false;
        if (name != null ? !name.equals(project.name) : project.name != null) return false;
        if (description != null ? !description.equals(project.description) : project.description != null) return false;
        if (webSiteUrl != null ? !webSiteUrl.equals(project.webSiteUrl) : project.webSiteUrl != null) return false;
        if (photoUrl != null ? !photoUrl.equals(project.photoUrl) : project.photoUrl != null) return false;
        if (requiredSkills != null ? !requiredSkills.equals(project.requiredSkills) : project.requiredSkills != null)
            return false;
        if (category != null ? !category.equals(project.category) : project.category != null) return false;
//        if (owners != null ? !owners.equals(project.owners) : project.owners != null) return false;
        if (rate != null ? !rate.equals(project.rate) : project.rate != null) return false;
        if (createdAt != null ? !createdAt.equals(project.createdAt) : project.createdAt != null) return false;
        if (projectNews != null ? !projectNews.equals(project.projectNews) : project.projectNews != null) return false;
        if (members != null ? !members.equals(project.members) : project.members != null) return false;
        if (necessarySkills != null ? !necessarySkills.equals(project.necessarySkills) : project.necessarySkills != null)
            return false;
        return tags != null ? tags.equals(project.tags) : project.tags == null;

    }

    @Override
    public int hashCode() {
        int result = description != null ? description.hashCode() : 0;
        result = 31 * result + (webSiteUrl != null ? webSiteUrl.hashCode() : 0);
        result = 31 * result + (photoUrl != null ? photoUrl.hashCode() : 0);
        result = 31 * result + (requiredSkills != null ? requiredSkills.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
//        result = 31 * result + (owners != null ? owners.hashCode() : 0);
        result = 31 * result + (rate != null ? rate.hashCode() : 0);
        result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
        result = 31 * result + (projectNews != null ? projectNews.hashCode() : 0);
        result = 31 * result + (members != null ? members.hashCode() : 0);
        result = 31 * result + (necessarySkills != null ? necessarySkills.hashCode() : 0);
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        return result;
    }
}
