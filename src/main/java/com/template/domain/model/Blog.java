package com.template.domain.model;

import com.template.domain.IdModel;
import jdk.nashorn.internal.objects.annotations.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.URL;
import org.springframework.data.annotation.CreatedBy;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by: Sergey Volokh
 * Date: 5/18/2016
 * Time: 2:47 PM
 * Project: Spring MVC
 */
@Entity
@Table(name = "blogs")
@Access(AccessType.FIELD)
public class Blog implements IdModel {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(name = "blog_topic_name", nullable = false)
    private String topic;

    @Column(name = "blog_description", nullable = false)
    private String description;

    @Column(name = "blog_content", nullable = false)
    private String content;

    @URL
    @Column(name = "blog_photo_url")
    private String photoUrl;

    @CreationTimestamp
    @Column(name = "created_at", unique = false)
    @Temporal(TemporalType.DATE)
    private Date createdAt;

    @JoinColumn(name = "category_id")
    @OneToOne(fetch = FetchType.EAGER)
    private Category category;

    @JoinColumn(name = "rate_id")
    @OneToOne(fetch = FetchType.EAGER)
    private Rate rate;

    @Column(name = "blog_tag")
    @OneToMany(fetch = FetchType.EAGER)
    private List<Tags> tags;

    @OneToMany
    private List<Comments> comments;

    public Blog(){}

    public Blog(Long id){
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Rate getRate() {
        return rate;
    }

    public void setRate(Rate rate) {
        this.rate = rate;
    }

    public List<Tags> getTags() {
        return tags;
    }

    public void setTags(List<Tags> tags) {
        this.tags = tags;
    }

    public List<Comments> getComments() {
        return comments;
    }

    public void setComments(List<Comments> comments) {
        this.comments = comments;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
