package com.template.domain.model;

import com.template.domain.IdModel;

import javax.persistence.*;

/**
 * Created by: Sergey Volokh
 * Date: 5/18/2016
 * Time: 4:18 PM
 * Project: Spring MVC
 */
@Entity
@Table(name = "skill")
@Access(AccessType.FIELD)
public class Skill implements IdModel {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "skill_name")
    private String name;

    public Skill() {
    }

    public Skill(Long id) {
        this.id = id;
    }

    public Skill(String name) {
        this.name = name;
    }

    public Skill(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
