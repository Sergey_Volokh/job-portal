package com.template.domain.repository;

import com.template.domain.model.Dialog;

/**
 * Created by: Sergey Volokh
 * Date: 6/2/2016
 * Time: 6:36 PM
 * Project: Diplom
 */
public interface DialogRepository extends RootRepository<Dialog, Long> {
}
