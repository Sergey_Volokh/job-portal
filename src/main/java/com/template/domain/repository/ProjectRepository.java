package com.template.domain.repository;

import com.template.domain.model.Project;
import com.template.domain.model.enums.CategoryType;

import java.util.List;

/**
 * Created by: Sergey Volokh
 * Date: 5/13/2016
 * Time: 7:00 PM
 * Project: Spring MVC
 */
public interface ProjectRepository extends RootRepository<Project, Long> {

    List<Project> findAllByCategoryName(String name);

    List<Project> findAllByCategoryNameAndCategoryType(String name, CategoryType project);

    List<Project> findAllByMembersId(Long id);
}