package com.template.domain.repository;

import com.template.domain.model.Skill;

import javax.transaction.Transactional;

/**
 * Created by: Sergey Volokh
 * Date: 5/13/2016
 * Time: 7:00 PM
 * Project: Spring MVC
 */
public interface SkillRepository extends RootRepository<Skill, Long> {

}
