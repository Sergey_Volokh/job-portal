package com.template.domain.repository;

import com.template.domain.model.News;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by: Sergey Volokh
 * Date: 5/25/2016
 * Time: 5:39 PM
 * Project: Diplom
 */
public interface NewsRepository extends RootRepository<News, Long>{
}
