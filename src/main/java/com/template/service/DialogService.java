package com.template.service;

import com.template.domain.model.Dialog;

/**
 * Created by: Sergey Volokh
 * Date: 6/2/2016
 * Time: 6:38 PM
 * Project: Diplom
 */
public interface DialogService extends RootService<Dialog> {
}
