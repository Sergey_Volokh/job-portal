package com.template.service;

import com.template.domain.model.Skill;

/**
 * Created by: Sergey Volokh
 * Date: 5/18/2016
 * Time: 5:02 PM
 * Project: Spring MVC
 */
public interface SkillService extends RootService<Skill> {
}
