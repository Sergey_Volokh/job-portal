package com.template.service;

import com.template.domain.model.News;

/**
 * Created by: Sergey Volokh
 * Date: 5/25/2016
 * Time: 5:40 PM
 * Project: Diplom
 */
public interface NewsService extends RootService<News> {
}
