package com.template.service;

import com.template.domain.model.Project;

import java.util.List;

/**
 * Created by: Sergey Volokh
 * Date: 5/18/2016
 * Time: 5:02 PM
 * Project: Spring MVC
 */
public interface ProjectService extends RootService<Project> {

    List<Project> findAllByCategoryName(String name);

    List<Project> findAllByMembersId(Long id);

}
