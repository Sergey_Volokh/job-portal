package com.template.service;

import com.template.domain.model.Category;
import com.template.domain.model.enums.CategoryType;

import java.util.List;

/**
 * Created by: Sergey Volokh
 * Date: 5/25/2016
 * Time: 7:31 PM
 * Project: Diplom
 */
public interface CategoryService extends RootService<Category>  {

    List<Category> findAllByType(CategoryType type);

}
