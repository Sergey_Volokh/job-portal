package com.template.service.impl;

import com.template.domain.model.Category;
import com.template.domain.model.enums.CategoryType;
import com.template.domain.repository.CategoryRepository;
import com.template.domain.repository.RootRepository;
import com.template.exception.NotFoundException;
import com.template.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by: Sergey Volokh
 * Date: 5/25/2016
 * Time: 7:32 PM
 * Project: Diplom
 */
@Service
@Transactional
public class CategoryServiceImpl extends RootServiceImpl<Category> implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public RootRepository<Category, Long> getRepository() {
        return categoryRepository;
    }

    @Override
    public Class<Category> getEntityClass() {
        return Category.class;
    }

    @Override
    public List<Category> findAllByType(CategoryType type) {
        return categoryRepository.findAllByType(type);
    }

}
