package com.template.service.impl;

import com.template.domain.model.Project;
import com.template.domain.model.enums.CategoryType;
import com.template.domain.repository.ProjectRepository;
import com.template.domain.repository.RootRepository;
import com.template.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by: Sergey Volokh
 * Date: 5/18/2016
 * Time: 5:03 PM
 * Project: Spring MVC
 */
@Service
@Transactional
public class ProjectServiceImpl extends RootServiceImpl<Project> implements ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public RootRepository<Project, Long> getRepository() {
        return projectRepository;
    }

    @Override
    public Class<Project> getEntityClass() {
        return Project.class;
    }

    @Override
    public List<Project> findAll(Sort sort) {
        return projectRepository.findAll(sort);
    }

    @Override
    public Page<Project> findAllPageable(Pageable paging) {
        return projectRepository.findAll(paging);
    }

    @Override
    public List<Project> findAllByCategoryName(String name) {
        return projectRepository.findAllByCategoryName(name);
    }

    @Override
    public List<Project> findAllByMembersId(Long id) {
        return projectRepository.findAllByMembersId(id);
    }
}
