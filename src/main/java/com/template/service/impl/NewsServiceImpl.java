package com.template.service.impl;

import com.template.domain.model.News;
import com.template.domain.repository.NewsRepository;
import com.template.domain.repository.RootRepository;
import com.template.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by: Sergey Volokh
 * Date: 5/25/2016
 * Time: 5:41 PM
 * Project: Diplom
 */
@Service
@Transactional
public class NewsServiceImpl extends RootServiceImpl<News> implements NewsService {

    @Autowired
    private NewsRepository newsRepository;

    @Override
    public Class<News> getEntityClass() {
        return News.class;
    }

    @Override
    public RootRepository<News, Long> getRepository() {
        return newsRepository;
    }

}
