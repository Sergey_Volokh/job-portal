package com.template.service.impl;

import com.template.domain.model.Skill;
import com.template.domain.repository.RootRepository;
import com.template.domain.repository.SkillRepository;
import com.template.service.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by: Sergey Volokh
 * Date: 5/18/2016
 * Time: 5:03 PM
 * Project: Spring MVC
 */
@Service
@Transactional
public class SkillServiceImpl extends RootServiceImpl<Skill> implements SkillService {

    @Autowired
    private SkillRepository skillRepository;

    @Override
    public RootRepository<Skill, Long> getRepository() {
        return skillRepository;
    }

    @Override
    public Class<Skill> getEntityClass() {
        return Skill.class;
    }

    @Override
    public List<Skill> findAll(Sort sort) {
        return skillRepository.findAll(sort);
    }

    @Override
    public Page<Skill> findAllPageable(Pageable paging) {
        return skillRepository.findAll(paging);
    }
}
