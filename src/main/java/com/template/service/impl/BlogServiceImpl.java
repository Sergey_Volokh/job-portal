package com.template.service.impl;

import com.template.domain.model.Blog;
import com.template.domain.model.enums.CategoryType;
import com.template.domain.repository.BlogRepository;
import com.template.domain.repository.RootRepository;
import com.template.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by: Sergey Volokh
 * Date: 5/18/2016
 * Time: 5:03 PM
 * Project: Spring MVC
 */
@Service
public class BlogServiceImpl extends RootServiceImpl<Blog> implements BlogService {

    @Autowired
    private BlogRepository blogRepository;

    @Override
    public RootRepository<Blog, Long> getRepository() {
        return blogRepository;
    }

    @Override
    public Class<Blog> getEntityClass() {
        return Blog.class;
    }

    @Override
    public List<Blog> findAll(Sort sort) {
        return blogRepository.findAll(sort);
    }

    @Override
    public Page<Blog> findAllPageable(Pageable paging) {
        return blogRepository.findAll(paging);
    }

    @Override
    public List<Blog> findAllByCategoryType(CategoryType categoryType) {
        return blogRepository.findAllByCategoryType(categoryType);
    }

    @Override
    public List<Blog> findAllByCategoryName(String name) {
        return blogRepository.findAllByCategoryName(name);
    }

    @Override
    public Blog findBlogByCategoryNameAndTopic(String catName, String topic) {
        return blogRepository.findBlogByCategoryNameAndTopic(catName, topic);
    }
}
