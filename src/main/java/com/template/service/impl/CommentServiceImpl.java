package com.template.service.impl;

import com.template.domain.model.Comments;
import com.template.domain.repository.CommentRepository;
import com.template.domain.repository.RootRepository;
import com.template.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by: Sergey Volokh
 * Date: 5/18/2016
 * Time: 5:03 PM
 * Project: Spring MVC
 */
@Service
@Transactional
public class CommentServiceImpl extends RootServiceImpl<Comments> implements CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Override
    public RootRepository<Comments, Long> getRepository() {
        return commentRepository;
    }

    @Override
    public Class<Comments> getEntityClass() {
        return Comments.class;
    }

    @Override
    public List<Comments> findAll(Sort sort) {
        return commentRepository.findAll(sort);
    }

    @Override
    public Page<Comments> findAllPageable(Pageable paging) {
        return commentRepository.findAll(paging);
    }
}
