package com.template.service.impl;

import com.template.domain.model.Dialog;
import com.template.domain.repository.DialogRepository;
import com.template.domain.repository.RootRepository;
import com.template.service.DialogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by: Sergey Volokh
 * Date: 6/2/2016
 * Time: 6:48 PM
 * Project: Diplom
 */
@Service
public class DialogServiceImpl extends RootServiceImpl<Dialog> implements DialogService {

    @Autowired
    private DialogRepository dialogRepository;

    @Override
    public RootRepository<Dialog, Long> getRepository() {
        return dialogRepository;
    }

    @Override
    public Class<Dialog> getEntityClass() {
        return Dialog.class;
    }
}
