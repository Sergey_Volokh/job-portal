package com.template.service;

import com.template.domain.model.User;

/**
 * Created by: Sergey Volokh
 * Date: 5/18/2016
 * Time: 5:02 PM
 * Project: Spring MVC
 */
public interface UserService extends RootService<User> {

    User registerNewUser(User user);

    User findByEmail(String email);

}
