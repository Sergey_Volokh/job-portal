package com.template.service;

import com.template.domain.model.Blog;
import com.template.domain.model.enums.CategoryType;

import java.util.List;

/**
 * Created by: Sergey Volokh
 * Date: 5/18/2016
 * Time: 5:02 PM
 * Project: Spring MVC
 */
public interface BlogService extends RootService<Blog> {

    List<Blog> findAllByCategoryType(CategoryType categoryType);
    List<Blog> findAllByCategoryName(String name);
    Blog findBlogByCategoryNameAndTopic(String catName, String topic);

}
