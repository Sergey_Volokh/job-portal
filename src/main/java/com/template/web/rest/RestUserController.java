package com.template.web.rest;

import com.template.domain.model.Project;
import com.template.domain.model.User;
import com.template.domain.model.enums.OnlineStatus;
import com.template.domain.model.enums.UserStatus;
import com.template.domain.repository.ProjectRepository;
import com.template.domain.repository.UserRepository;
import com.template.service.ProjectService;
import com.template.service.UserService;
import com.template.web.JsonResponse;
import com.template.web.form.ProjectForm;
import com.template.web.form.UserForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by: Sergey Volokh
 * Date: 5/20/2016
 * Time: 2:18 PM
 * Project: Spring MVC
 */
@RestController
@RequestMapping(value = "/rest/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class RestUserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ProjectService projectService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getCurrentUserProfile(){
        User user = userService.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
        UserForm form = new UserForm().getForm(user);
        List<Project> projects = projectService.findAllByMembersId(user.getId());
        form.setTeamMemberProjects(projects.stream().map((p) -> new ProjectForm().getForm(p)).collect(Collectors.toList()));

        return new ResponseEntity<>(form, HttpStatus.OK);
    }

    @RequestMapping(value = "/all-people", method = RequestMethod.GET)
    public ResponseEntity<?> getAllUserProfile(){
        List<User> users = userService.findAll(new Sort(Sort.Direction.ASC, "nickName"));
        List<UserForm> forms = users.stream().map((u) ->{
            UserForm form = new UserForm();
            form.setId(u.getId());
            form.setFirstName(u.getFirstName());
            form.setLastName(u.getLastName());
            form.setNickName(u.getNickName());
            form.setPhotoUrl(u.getPhotoUrl());
            form.setDescription(u.getDescription());
            form.setJobName(u.getJobName());
            return form;
        }).collect(Collectors.toList());

        return new ResponseEntity<>(forms, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<?> updateUserAccount(@RequestBody @Valid UserForm form){
        User user = userService.get(form.getId());
        form.setPassword(user.getPassword());
        user = form.getEntity();
        userService.update(user);
        return new ResponseEntity<>(form, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> addUserAccount(@RequestBody @Valid UserForm user){
        userService.insert(user.getEntity());
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @RequestMapping(value = "/add/project/{id}", method = RequestMethod.POST)
    public ResponseEntity<?> addUserProject(@PathVariable("id") Long id){
        User user = userService.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName());

        Project project = projectService.get(id);
        if(!user.getOwnerProjects().contains(project)){
            project.getMembers().add(user);
            projectService.update(project);
        }

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteUserAccount(@RequestBody @Valid UserForm user){
        userService.delete(user.getEntity().getId());
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

}
