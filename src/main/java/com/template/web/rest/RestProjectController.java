package com.template.web.rest;

import com.template.domain.model.*;
import com.template.domain.model.enums.CategoryType;
import com.template.service.*;
import com.template.web.form.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;

/**
 * Created by: Sergey Volokh
 * Date: 5/25/2016
 * Time: 5:38 PM
 * Project: Diplom
 */
@RestController
@RequestMapping(value = "/rest/projects", produces = MediaType.APPLICATION_JSON_VALUE)
public class RestProjectController {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private NewsService newsService;

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getAllProject() {
        return new ResponseEntity<>(
                projectService.findAll(new Sort(Sort.Direction.DESC, "createdAt"))
                        .stream().map((p) -> new ProjectForm().getForm(p))
                        .collect(Collectors.toList())
                , HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getProjectById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(new ProjectForm().getForm(projectService.get(id)), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> addProject(@RequestBody ProjectForm form) {
        Project project = form.getEntity();
        User user = userService.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
        user.getOwnerProjects().add(project);
        project = projectService.insert(project);
        return new ResponseEntity<>(form.getForm(project), HttpStatus.OK);
    }

    @RequestMapping(value = "/add/news", method = RequestMethod.POST)
    public ResponseEntity<?> addProjectNews(@RequestBody NewsForm form) {
        Project project = projectService.get(form.getProjectId());
        News news = newsService.insert(form.getEntity());
        project.getProjectNews().add(news);
        projectService.update(project);

        return new ResponseEntity<>(form.getForm(news), HttpStatus.OK);
    }

    @RequestMapping(value = "/news/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getProjectNews(@PathVariable("id") Long id) {
        NewsForm news = new NewsForm().getForm(newsService.get(id));
        return new ResponseEntity<>(news, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<?> updateProject(@RequestBody ProjectForm form) {
        Project project = projectService.update(form.getEntity());
        return new ResponseEntity<>(form.getForm(project), HttpStatus.OK);
    }

    @RequestMapping(value = "/categories/{name}", method = RequestMethod.GET)
    public ResponseEntity<?> getProjectType(@PathVariable("name") String name) {
        return new ResponseEntity<>(projectService.findAllByCategoryName(name)
                .stream().map((p) -> new ProjectForm().getForm(p))
                .collect(Collectors.toList())
                , HttpStatus.OK);
    }

    @RequestMapping(value = "/comments", method = RequestMethod.POST)
    public ResponseEntity<?> postNewComment(@RequestBody CommentsForm form) {
        News news = newsService.get(form.getNewsId());

        Comments comment = commentService.insert(form.getEntity());
        comment.setDate(new Date(System.currentTimeMillis()));
        news.getComments().add(comment);

        newsService.insert(news);

        return new ResponseEntity<>(new CommentsForm().getForm(comment), HttpStatus.OK);
    }

}
