package com.template.web.rest;

import com.template.domain.model.enums.CategoryType;
import com.template.service.CategoryService;
import com.template.web.form.CategoryForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Collectors;

/**
 * Created by: Sergey Volokh
 * Date: 5/25/2016
 * Time: 7:44 PM
 * Project: Diplom
 */
@RestController
@RequestMapping(value = "/rest/categories", produces = MediaType.APPLICATION_JSON_VALUE)
public class RestCategoriesController {

    @Autowired
    private CategoryService categoryService;

    @RequestMapping(value = "/blogs", method = RequestMethod.GET)
    public ResponseEntity<?> getBlogCategory(){
        return new ResponseEntity<>(categoryService.findAllByType(CategoryType.BLOG)
                .stream().map((c) -> new CategoryForm().getForm(c))
                .collect(Collectors.toList())
                , HttpStatus.OK);
    }

    @RequestMapping(value = "/projects", method = RequestMethod.GET)
    public ResponseEntity<?> getProjectsCategory(){
        return new ResponseEntity<>(categoryService.findAllByType(CategoryType.PROJECT)
                .stream().map((c) -> new CategoryForm().getForm(c))
                .collect(Collectors.toList())
                , HttpStatus.OK);
    }

//    @RequestMapping(value = "/news", method = RequestMethod.GET)
//    public ResponseEntity<?> getNewsCategory(){
//        return new ResponseEntity<>(categoryService.findAllByType(CategoryType.NEWS)
//                .stream().map((c) -> new CategoryForm().getForm(c))
//                .collect(Collectors.toList())
//                , HttpStatus.OK);
//    }

}
