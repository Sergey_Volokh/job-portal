package com.template.web.rest;

import com.template.domain.model.Blog;
import com.template.domain.model.Category;
import com.template.domain.model.Comments;
import com.template.domain.model.enums.CategoryType;
import com.template.service.BlogService;
import com.template.service.CategoryService;
import com.template.service.CommentService;
import com.template.web.form.BlogForm;
import com.template.web.form.CategoryForm;
import com.template.web.form.CommentsForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by: Sergey Volokh
 * Date: 5/13/2016
 * Time: 7:48 PM
 * Project: Spring MVC
 */
@RestController
@RequestMapping(value = "/rest/blogs", produces = MediaType.APPLICATION_JSON_VALUE)
public class RestBlogController {

    @Autowired
    private BlogService blogService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CommentService commentService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getAllBlogCategory() {
        List<Category> blogCategories = categoryService.findAllByType(CategoryType.BLOG);
        return new ResponseEntity<>(blogCategories.stream().map((c) -> new CategoryForm().getForm(c)).collect(Collectors.toList()), HttpStatus.OK);
    }

    @RequestMapping(value = "/one/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getBlog(@PathVariable("id") Long id) {
        Blog blog = blogService.get(id);
        return new ResponseEntity<>(new BlogForm().getForm(blog), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> postNewBlog(@RequestBody @Valid BlogForm newBlog) {
        Blog blog = blogService.insert(newBlog.getEntity());
        return new ResponseEntity<>(newBlog.getForm(blog), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<?> updateBlog(@RequestBody @Valid BlogForm form){
        blogService.update(form.getEntity());
        return new ResponseEntity<>("Successfully updated blog.", HttpStatus.OK);
    }

    @RequestMapping(value = "/comments", method = RequestMethod.POST)
    public ResponseEntity<?> postNewComment(@RequestBody CommentsForm form) {
        Blog blog =  blogService.get(form.getBlogId());

        Comments comment = commentService.insert(form.getEntity());
        comment.setDate(new Date(System.currentTimeMillis()));
        blog.getComments().add(comment);

        blogService.insert(blog);

        return new ResponseEntity<>(new CommentsForm().getForm(comment), HttpStatus.OK);
    }

    @RequestMapping(value = "/categories/{categoryName}", method = RequestMethod.GET)
    public ResponseEntity<?> getBlogByCategory(@PathVariable("categoryName") String categoryName) {
        List<Blog> blog = blogService.findAllByCategoryName(categoryName);
        return new ResponseEntity<>(blog
                .stream().map((b) -> new BlogForm().getForm(b)).collect(Collectors.toList()), HttpStatus.OK);
    }

    @RequestMapping(value = "/categories", method = RequestMethod.GET)
    public ResponseEntity<?> getBlogByCategory() {
        List<Category> categories = categoryService.findAllByType(CategoryType.BLOG);
        return new ResponseEntity<>(categories
                .stream().map((c) -> new CategoryForm().getForm(c))
                .collect(Collectors.toList()), HttpStatus.OK);
    }

}