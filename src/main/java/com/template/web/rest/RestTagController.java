package com.template.web.rest;

import com.template.domain.model.Skill;
import com.template.domain.model.Tags;
import com.template.service.TagService;
import com.template.web.form.SkillForm;
import com.template.web.form.TagsForm;
import com.template.web.form.UserForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created by: Sergey Volokh
 * Date: 6/3/2016
 * Time: 8:24 PM
 * Project: springmvcs
 */
@RestController
@RequestMapping(value = "/rest/tags", produces = MediaType.APPLICATION_JSON_VALUE)
public class RestTagController {

    @Autowired
    private TagService tagService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getAllTags(){
        return new ResponseEntity<>(tagService.findAll(new Sort(Sort.Direction.ASC, "name")), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<?> updateTag(@RequestBody @Valid TagsForm form){
        Tags tags = tagService.update(form.getEntity());
        return new ResponseEntity<>(tags, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> addTag(@RequestBody @Valid TagsForm form){
        Tags tags = tagService.insert(form.getEntity());
        return new ResponseEntity<>(tags, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteTag(@RequestBody @Valid TagsForm form){
        boolean tagsDeleted = tagService.delete(form.getEntity().getId());
        if(tagsDeleted){
            return new ResponseEntity<>("Tag " + form + " deleted successfully.", HttpStatus.OK);
        }else {
            return new ResponseEntity<>("Tag " + form + " deleted error.", HttpStatus.OK);
        }
    }

}
