package com.template.web.rest;

import com.template.domain.model.Dialog;
import com.template.service.DialogService;
import com.template.service.UserService;
import com.template.web.form.DialogForm;
import com.template.web.form.MessageForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by: Sergey Volokh
 * Date: 6/8/2016
 * Time: 6:56 PM
 * Project: springmvcs
 */
@RestController
@RequestMapping(value = "/rest/messages",
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE)
public class RestMessageController {

    @Autowired
    private UserService userService;

    @Autowired
    private DialogService dialogService;

    @RequestMapping(value = "/current-user-dialogs", method = RequestMethod.GET)
    public ResponseEntity<?> getDialogCurrentUser(){
        List<Dialog> dialogs = userService.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName()).getDialogs();
        return new ResponseEntity<>(
                dialogs.stream().map((d) -> new DialogForm().getForm(d)).collect(Collectors.toList())
                , HttpStatus.OK);
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    public ResponseEntity<?> getDialogByUserId(@PathVariable("userId") Long id){
        List<Dialog> dialogs = userService.get(id).getDialogs();
        return new ResponseEntity<>(
                dialogs.stream().map((d) -> new DialogForm().getForm(d)).collect(Collectors.toList())
                , HttpStatus.OK);
    }

    @RequestMapping(value = "/dialogs/{dialogId}", method = RequestMethod.GET)
    public ResponseEntity<?> getMessagesByDialog(@PathVariable("dialogId") Long id){
        return new ResponseEntity<>(
                dialogService.get(id).getMessages().stream().map((m) -> new MessageForm().getForm(m))
                , HttpStatus.OK);
    }



}
