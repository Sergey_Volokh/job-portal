package com.template.web.rest;

import com.template.domain.model.Skill;
import com.template.service.SkillService;
import com.template.web.form.SkillForm;
import com.template.web.form.UserForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created by: Sergey Volokh
 * Date: 6/3/2016
 * Time: 8:24 PM
 * Project: springmvcs
 */
@RestController
@RequestMapping(value = "/rest/skills", produces = MediaType.APPLICATION_JSON_VALUE)
public class RestSkillController {

    @Autowired
    private SkillService skillService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getAllSkill(){
        return new ResponseEntity<>(skillService.findAll(new Sort(Sort.Direction.ASC, "name")), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<?> updateSkill(@RequestBody @Valid SkillForm form){
        Skill skill = skillService.update(form.getEntity());
        return new ResponseEntity<>(skill, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> addSkill(@RequestBody @Valid SkillForm form){
        Skill skill = skillService.insert(form.getEntity());
        return new ResponseEntity<>(skill, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteSkill(@RequestBody @Valid SkillForm form){
        boolean skillDeleted = skillService.delete(form.getEntity().getId());
        if(skillDeleted){
            return new ResponseEntity<>("Skill " + form + " deleted successfully.", HttpStatus.OK);
        }else {
            return new ResponseEntity<>("Tag " + form + " deleted error.", HttpStatus.OK);
        }
    }



}
