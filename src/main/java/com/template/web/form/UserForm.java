package com.template.web.form;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.template.domain.model.Project;
import com.template.domain.model.User;
import com.template.domain.model.enums.OnlineStatus;
import com.template.domain.model.enums.UserStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by: Sergey Volokh
 * Date: 5/21/2016
 * Time: 3:23 PM
 * Project: Spring MVC
 */
public class UserForm extends BaseForm<User> {

    private String firstName;

    private String lastName;

    private String email;

    @JsonIgnore
    private String password;

    private String nickName;

    private String photoUrl;

    private Date birthDay;

    private String description;

    private String jobName;

    private UserStatus userStatus = UserStatus.INACTIVE;

    private OnlineStatus onlineStatus = OnlineStatus.OFFLINE;

    private List<ProjectForm> ownerProjects = new ArrayList<>();

    private List<ProjectForm> teamMemberProjects = new ArrayList<>();

    @Override
    public User getEntity() {
        User user = new User();
        user.setId(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setPassword(password);
        user.setBirthDay(birthDay);
        user.setNickName(nickName);
        user.setPhotoUrl(photoUrl);
        user.setJobName(jobName);
        user.setDescription(description);
        user.setUserStatus(userStatus);
        user.setOnlineStatus(onlineStatus);
        if(!ownerProjects.isEmpty())
            user.setOwnerProjects(ownerProjects.stream().map(ProjectForm::getEntity).collect(Collectors.toList()));
//        if(!teamMemberProjects.isEmpty())
//            user.setTeamMemberProjects(teamMemberProjects.stream().map((m) -> m.getEntity()).collect(Collectors.toList()));

        return user;
    }

    @Override
    public UserForm getForm(User entity) {
        UserForm form = new UserForm();
        form.setId(entity.getId());
        form.setFirstName(entity.getFirstName());
        form.setLastName(entity.getLastName());
        form.setNickName(entity.getNickName());
        form.setPhotoUrl(entity.getPhotoUrl());
        form.setPassword(entity.getPassword());
        form.setBirthDay(entity.getBirthDay());
        form.setEmail(entity.getEmail());
        form.setDescription(entity.getDescription());
        form.setJobName(entity.getJobName());
        form.setUserStatus(entity.getUserStatus());
        form.setOnlineStatus(entity.getOnlineStatus());
        if(entity.getOwnerProjects() != null)
            form.setOwnerProjects(entity.getOwnerProjects().stream().map((o) -> new ProjectForm().getForm(o)).collect(Collectors.toList()));
//        if(entity.getTeamMemberProjects() != null)
//            form.setTeamMemberProjects(entity.getTeamMemberProjects().stream().map((o) -> new ProjectForm().getForm(o)).collect(Collectors.toList()));

        return form;
    }

    public UserForm() {
    }

    public UserForm(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    public OnlineStatus getOnlineStatus() {
        return onlineStatus;
    }

    public void setOnlineStatus(OnlineStatus onlineStatus) {
        this.onlineStatus = onlineStatus;
    }

    public List<ProjectForm> getOwnerProjects() {
        return ownerProjects;
    }

    public void setOwnerProjects(List<ProjectForm> ownerProjects) {
        this.ownerProjects = ownerProjects;
    }

    public List<ProjectForm> getTeamMemberProjects() {
        return teamMemberProjects;
    }

    public void setTeamMemberProjects(List<ProjectForm> teamMemberProjects) {
        this.teamMemberProjects = teamMemberProjects;
    }

    @Override
    public String toString() {
        return "UserForm{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", nickName='" + nickName + '\'' +
                ", birthDay=" + birthDay +
                ", description='" + description + '\'' +
                ", userStatus=" + userStatus +
                ", onlineStatus=" + onlineStatus +
                '}';
    }
}
