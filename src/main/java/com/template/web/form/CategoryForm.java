package com.template.web.form;

import com.template.domain.model.Category;
import com.template.domain.model.enums.CategoryType;

/**
 * Created by: Sergey Volokh
 * Date: 5/21/2016
 * Time: 3:44 PM
 * Project: Spring MVC
 */
public class CategoryForm extends BaseForm<Category> {

    private String name;

    private String description;

    private String photoUrl;

    private CategoryType type;

    @Override
    public Category getEntity() {
        Category category = new Category();
        category.setId(id);
        category.setName(name);
        category.setDescription(description);
        category.setPhotoUrl(photoUrl);
        category.setType(type);

        return category;
    }

    @Override
    public CategoryForm getForm(Category entity) {
        CategoryForm form = new CategoryForm();
        form.setId(entity.getId());
        form.setName(entity.getName());
        form.setDescription(entity.getDescription());
        form.setPhotoUrl(entity.getPhotoUrl());
        form.setType(entity.getType());

        return form;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public CategoryType getType() {
        return type;
    }

    public void setType(CategoryType type) {
        this.type = type;
    }
}
