package com.template.web.form;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.template.domain.model.Category;
import com.template.domain.model.Project;
import com.template.domain.model.enums.CategoryType;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by: Sergey Volokh
 * Date: 5/21/2016
 * Time: 3:50 PM
 * Project: Spring MVC
 */
public class ProjectForm extends BaseForm<Project> {

    private String name;

    private String description;

    private String photoUrl;

    private String webSiteUrl;

    private String requiredSkills;

    private CategoryForm category;

    @JsonIgnore
    private List<UserForm> owners = new ArrayList<>();

    private List<UserForm> members = new ArrayList<>();

    private List<TagsForm> tags = new ArrayList<>();

    private List<SkillForm> necessarySkills = new ArrayList<>();

    private List<NewsForm> projectNews = new ArrayList<>();

    @Override
    public Project getEntity() {
        Project project = new Project();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        project.setPhotoUrl(photoUrl);
        project.setWebSiteUrl(webSiteUrl);
        project.setRequiredSkills(requiredSkills);
        if (category != null)
            project.setCategory(category.getEntity());
//        if (!owners.isEmpty())
//            project.setOwners(owners.stream().map((o) -> o.getEntity()).collect(Collectors.toList()));
        if (!members.isEmpty())
            project.setMembers(members.stream().map((m) -> m.getEntity()).collect(Collectors.toList()));
        if (!tags.isEmpty())
            project.setTags(tags.stream().map((t) -> t.getEntity()).collect(Collectors.toList()));
        if (!necessarySkills.isEmpty())
            project.setNecessarySkills(necessarySkills.stream().map((s) -> s.getEntity()).collect(Collectors.toList()));
        if (!projectNews.isEmpty())
            project.setProjectNews(projectNews.stream().map((n) -> n.getEntity()).collect(Collectors.toList()));

        return project;
    }

    @Override
    public ProjectForm getForm(Project entity) {
        ProjectForm form = new ProjectForm();
        form.setId(entity.getId());
        form.setName(entity.getName());
        form.setDescription(entity.getDescription());
        form.setPhotoUrl(entity.getPhotoUrl());
        form.setWebSiteUrl(entity.getWebSiteUrl());
        form.setRequiredSkills(entity.getRequiredSkills());
//        if (entity.getOwners() != null && !entity.getOwners().isEmpty())
//            form.setOwners(entity.getOwners().stream().map((o) -> new UserForm().getForm(o)).collect(Collectors.toList()));
        if (entity.getMembers() != null)
            form.setMembers(entity.getMembers().stream().map((m) -> new UserForm().getForm(m)).collect(Collectors.toList()));
        if (entity.getTags() != null)
            form.setTags(entity.getTags().stream().map((t) -> new TagsForm().getForm(t)).collect(Collectors.toList()));
        if (entity.getNecessarySkills() != null)
            form.setNecessarySkills(entity.getNecessarySkills().stream().map((s) -> new SkillForm().getForm(s)).collect(Collectors.toList()));
        if (entity.getProjectNews() != null)
            form.setProjectNews(entity.getProjectNews().stream().map((n) -> new NewsForm().getForm(n)).collect(Collectors.toList()));

        return form;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getWebSiteUrl() {
        return webSiteUrl;
    }

    public void setWebSiteUrl(String webSiteUrl) {
        this.webSiteUrl = webSiteUrl;
    }

    public String getRequiredSkills() {
        return requiredSkills;
    }

    public void setRequiredSkills(String requiredSkills) {
        this.requiredSkills = requiredSkills;
    }

    public List<UserForm> getOwners() {
        return owners;
    }

    public void setOwners(List<UserForm> owners) {
        this.owners = owners;
    }

    public CategoryForm getCategory() {
        return category;
    }

    public void setCategory(CategoryForm category) {
        this.category = category;
    }

    public List<UserForm> getMembers() {
        return members;
    }

    public void setMembers(List<UserForm> members) {
        this.members = members;
    }

    public List<TagsForm> getTags() {
        return tags;
    }

    public void setTags(List<TagsForm> tags) {
        this.tags = tags;
    }

    public List<SkillForm> getNecessarySkills() {
        return necessarySkills;
    }

    public void setNecessarySkills(List<SkillForm> necessarySkills) {
        this.necessarySkills = necessarySkills;
    }

    public List<NewsForm> getProjectNews() {
        return projectNews;
    }

    public void setProjectNews(List<NewsForm> projectNews) {
        this.projectNews = projectNews;
    }
}
