package com.template.web.form;

import com.template.domain.model.Dialog;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by: Sergey Volokh
 * Date: 6/8/2016
 * Time: 7:02 PM
 * Project: springmvcs
 */
public class DialogForm extends BaseForm<Dialog> {

    private String name;

    private List<MessageForm> messages = new ArrayList<>();

    @Override
    public Dialog getEntity() {
        Dialog dialog = new Dialog();
        dialog.setName(name);
        dialog.setMessages(messages.stream().map((m) -> new MessageForm().getEntity()).collect(Collectors.toList()));
        return dialog;
    }

    @Override
    public DialogForm getForm(Dialog entity) {
        DialogForm form = new DialogForm();
        form.setId(id);
        form.setName(name);
        if (entity.getMessages() != null)
            form.setMessages(entity.getMessages().stream().map((m) -> new MessageForm().getForm(m)).collect(Collectors.toList()));

        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MessageForm> getMessages() {
        return messages;
    }

    public void setMessages(List<MessageForm> messages) {
        this.messages = messages;
    }
}
