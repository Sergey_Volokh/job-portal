package com.template.web.form;

import com.template.domain.model.Blog;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by: Sergey Volokh
 * Date: 5/21/2016
 * Time: 3:05 PM
 * Project: Spring MVC
 */
public class BlogForm extends BaseForm<Blog> {

    private String topic;

    private String description;

    private String content;

    private String photoUrl;

    private CategoryForm category;

    private List<TagsForm> tags = new ArrayList<>();

    private List<CommentsForm> comments = new ArrayList<>();

    @Override
    public Blog getEntity() {
        Blog blog = new Blog();
        blog.setId(id);
        blog.setTopic(topic);
        blog.setDescription(description);
        blog.setContent(content);
        blog.setPhotoUrl(photoUrl);
        if (category != null)
            blog.setCategory(category.getEntity());
        if (!tags.isEmpty())
            blog.setTags(tags.stream().map((t) -> t.getEntity()).collect(Collectors.toList()));
        if (!comments.isEmpty())
            blog.setComments(comments.stream().map((c) -> c.getEntity()).collect(Collectors.toList()));

        return blog;
    }

    @Override
    public BlogForm getForm(Blog entity) {
        BlogForm blogForm = new BlogForm();
        blogForm.setId(entity.getId());
        blogForm.setTopic(entity.getTopic());
        blogForm.setDescription(entity.getDescription());
        blogForm.setContent(entity.getContent());
        blogForm.setPhotoUrl(entity.getPhotoUrl());
        if (entity.getTags() != null)
            blogForm.setTags(entity.getTags().stream().map((t) -> new TagsForm().getForm(t)).collect(Collectors.toList()));
        if (entity.getComments() != null)
            blogForm.setComments(entity.getComments().stream().map((t) -> new CommentsForm().getForm(t)).collect(Collectors.toList()));
        return blogForm;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public CategoryForm getCategory() {
        return category;
    }

    public void setCategory(CategoryForm category) {
        this.category = category;
    }

    public List<TagsForm> getTags() {
        return tags;
    }

    public void setTags(List<TagsForm> tags) {
        this.tags = tags;
    }

    public List<CommentsForm> getComments() {
        return comments;
    }

    public void setComments(List<CommentsForm> comments) {
        this.comments = comments;
    }
}
