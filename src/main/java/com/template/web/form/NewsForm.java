package com.template.web.form;

import com.template.domain.model.News;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by: Sergey Volokh
 * Date: 6/6/2016
 * Time: 9:52 PM
 * Project: springmvcs
 */
public class NewsForm extends BaseForm<News> {

    private Long projectId;

    private String topic;

    private String description;

    private String content;

    private String photoUrl;

    private RateForm rate;

    private List<CommentsForm> comments = new ArrayList<>();

    @Override
    public News getEntity() {
        News news = new News();
        news.setId(id);
        news.setTopic(topic);
        news.setDescription(description);
        news.setContent(content);
        news.setPhotoUrl(photoUrl);
        if (rate != null)
            news.setRate(rate.getEntity());
        if (comments.isEmpty())
            news.setComments(comments.stream().map((c)->c.getEntity()).collect(Collectors.toList()));

        return news;
    }

    @Override
    public NewsForm getForm(News entity) {
        NewsForm form = new NewsForm();
        form.setId(entity.getId());
        form.setDescription(entity.getDescription());
        form.setTopic(entity.getTopic());
        form.setContent(entity.getContent());
        form.setPhotoUrl(entity.getPhotoUrl());
        if(entity.getRate() != null)
            form.setRate(new RateForm().getForm(entity.getRate()));
        if(entity.getComments() != null)
           form.setComments(entity.getComments().stream().map((c) -> new CommentsForm().getForm(c)).collect(Collectors.toList()));

        return form;
    }


    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public RateForm getRate() {
        return rate;
    }

    public void setRate(RateForm rate) {
        this.rate = rate;
    }

    public List<CommentsForm> getComments() {
        return comments;
    }

    public void setComments(List<CommentsForm> comments) {
        this.comments = comments;
    }
}
