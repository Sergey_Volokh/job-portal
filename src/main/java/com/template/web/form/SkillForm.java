package com.template.web.form;

import com.template.domain.model.Skill;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * Created by: Sergey Volokh
 * Date: 5/21/2016
 * Time: 3:55 PM
 * Project: Spring MVC
 */
public class SkillForm extends BaseForm<Skill> {

    private String name;

    @Override
    public Skill getEntity() {
        return new Skill(id ,name);
    }

    @Override
    public SkillForm getForm(Skill entity) {
        return new SkillForm(entity.getId(), entity.getName());
    }

    public SkillForm(){}

    public SkillForm(String name) {
        this.name = name;
    }

    public SkillForm(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
