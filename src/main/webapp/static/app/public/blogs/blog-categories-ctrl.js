angular.module('myApp.blog')
    .controller('BlogCategoryCtrl', ['$scope', '$timeout', '$routeParams', '$compile', 'BlogService', function ($scope, $timeout, $routeParams, $compile, BlogService) {

        $scope.blogs = [];

        $scope.getAllBlogByCategory = function(){
            BlogService.getAllBlogCategory({categoryName: $routeParams.category}, function(responce){
                $scope.blogs = responce;
                console.log(responce)
            }, function(error){
                console.log("BlogCategoryCtrl: GET All Blog By Category Error!!");
                console.log(error)
            })
        };

        $scope.getAllBlogByCategory();

    }]);