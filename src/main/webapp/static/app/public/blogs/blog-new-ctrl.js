angular.module('myApp.blog')
    .controller('BlogNewCtrl', ['$scope', '$timeout', '$location', 'CONTEXT', 'API', 'BlogService', function ($scope, $timeout, $location, CONTEXT, API, BlogService) {
        $scope.blogCategory = {};
        $scope.blogEntity = {
            topic: "",
            category: "",
            photoUrl: "",
            description: "",
            content: ""
        };

        $scope.postNewBlog = function () {
            BlogService.save($scope.blogEntity, function (blog) {
                console.log(blog);
                $location.path("/blog/" + blog.id)
            }, function (error) {
                console.log("BlogNewCtrl: Post New Blog Error!!");
                console.log(error)
            });
        };

        $scope.categories = BlogService.categories({}, function (responce) {
            $scope.blogEntity.category = $scope.categories[0];
        });

        $scope.onSelectCategory = function (model, item) {
            $scope.blogCategory = item;
        }
}]);