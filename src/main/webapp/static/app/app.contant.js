angular.module("myApp.config",[])
    .constant("API", {
        indexPath:"/",
        userPath:"/rest/users",
        blogPath:"/rest/blogs",
        newsPath:"/rest/news",
        projectPath:"/rest/projects",
        categoriesPath:"/rest/categories",
        categoriesProjectPath:"/rest/categories/projects",
        categoriesBlogPath:"/rest/categories/blog",
        categoriesNewsPath:"/rest/categories/news",
        skillsPath:"/rest/skills",
        tagsPath:"/rest/tags"
    })
    .factory('CONTEXT', ['$window',
        function($window) {
            var url = $window.location.pathname;
            return url.substring(0, url.lastIndexOf('/'));
        }
    ]);